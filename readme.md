## 中启乘数科技发行的PostreSQL数据库发行版本

由[中启乘数科技](http://www.csudata.com) 编译发行的版本。


## 编译环境

### For el7

| 项目名称 | 说明                  |
| -------- | --------------------- |
| CPU平台  | x86_64                |
| 操作系统 | CentOS7               |
| 内核版本 | 3.10.0-957.el7.x86_64 |
| 适用系统 | CentOS7               |

### For el8 

| 项目名称 | 说明                      |
| -------- | ------------------------- |
| CPU平台  | x86_64                    |
| 操作系统 | CentOS8                   |
| 内核版本 | 4.18.0-372.9.1.el8.x86_64 |
| 适用系统 | CentOS8、Rocky8           |

### 安装依赖项
由于PostgreSQL11及以上版本使用了LLVM，机器中需要安装依赖项
```sh
yum install -y llvm.x86_64 llvm-libs.x86_64 libicu
```

## 使用说明
移步到此处下载 [csupg 发行版 - Gitee.com](https://gitee.com/csudata/csupg/releases)


### 安装方式一：bin文件

执行命令，自动解压

```sh
bash csupg-x.bin
# csupg-x会被解压到/usr下面
```


### 安装方式二：tar.gz 文件

将其下载到/usr下解压即可，当然也可以是其他的目录下，这只是一个习惯。



## 相关信息

### PG版本

| 序号 | 版本号 | 备注 |
| ---- | ------ | ---- |
| 1    | 10.23  |      |
| 2    | 11.21  |      |
| 3    | 12.16  |      |
| 4    | 13.12  |      |
| 5    | 14.9   |      |

### 已安装的插件

| 序号 | 插件名称           | 备注            |
| ---- | ------------------ | --------------- |
| 1    | plproxy            | 水平分库工具    |
| 2    | pg_cron            | PG的定时任务    |
| 3    | pg_show_plans      | 查看SQL执行计划 |
| 4    | pg_store_plans     | 执行计划统计    |
| 5    | pg_hint_plans      | 执行计划优化器  |
| 6    | plpgsql_check      | plpgsql语法检查 |
| 7    | pg_stat_statements | 获取TOPSQL      |

<p style='color: orange;font-size: 20px;'>使用pg_cron时注意</p>

```sh
# 使用前需要先修改数据库的配置文件$PGDATA/postgresql.conf

# shared_preload_libraries中添加pg_cron
shared_preload_libraries = 'pg_stat_statements, pg_store_plans, pg_cron'
# 指定使用pg_cron的数据库
cron.database_name = 'clup'
# 指定主机，可以不填此项
cron.host = '127.0.0.1'
```


### 数据库版本和插件版本之间的对应关系

| 序号 | 插件名称           | PG10   | PG11   | PG12   | PG13   | PG14   |
| ---- | ------------------ | ------ | ------ | ------ | ------ | ------ |
| 1    | plproxy            | 2.10.0 | 2.10.0 | 2.10.0 | 2.10.0 | 2.10.0 |
| 2    | pg_cron            | 1.2    | 1.2    | 1.3    | 1.3    | 1.3    |
| 3    | pg_show_plans      | 1.0.0  | 1.0.1  | 1.0.1  | 1.0.1  | 1.0.1  |
| 4    | pg_store_plans     | 1.5    | 1.5    | 1.6.1  | 1.6.1  | 1.6.1  |
| 5    | pg_hint_plans      | 1.3.7  | 1.3.8  | 1.3.8  | 1.3.8  | 1.4.1  |
| 6    | plpgsql_check      | 2.3.4  | 2.3.4  | 2.4.0  | 2.4.0  | 2.4.0  |
| 7    | pg_stat_statements | 1.6    | 1.6    | 1.7    | 1.8    | 1.9    |

### 数据库的编译参数

PostgreSQL 10.23

```sh
./configure --enable-rpath --prefix=/usr/csupg-10.23 --datadir=/usr/csupg-10.23/share --includedir=/usr/csupg-10.23/include --mandir=/usr/csupg-10.23/share/man --libdir=/usr/csupg-10.23/lib --sysconfdir=/etc/sysconfig/csupg --docdir=/usr/csupg-10.23/doc --htmldir=/usr/csupg-10.23/doc/html --with-icu --with-tcl --with-perl --with-python --with-pam --with-openssl --with-libxml --with-libxslt --with-extra-version="\ (CSUDATA.COM CentOS 7)" --enable-nls  --enable-integer-datetimes --enable-thread-safety --enable-tap-tests --enable-debug --enable-dtrace --disable-rpath --with-uuid=e2fs --with-gnu-ld --with-system-tzdata=/usr/share/zoneinfo --with-systemd --with-selinux --with-gssapi --with-ldap --with-includes=/usr/include/mit-krb5 --with-libs=/usr/lib/mit-krb5 PYTHON='/usr/bin/python3'  --with-libs=/usr/lib/x86_64-linux-gnu/mit-krb5
```

PostgreSQL 11.21

```sh
./configure --enable-rpath --prefix=/usr/csupg-11.21 --datadir=/usr/csupg-11.21/share --includedir=/usr/csupg-11.21/include --mandir=/usr/csupg-11.21/share/man --libdir=/usr/csupg-11.21/lib --sysconfdir=/etc/sysconfig/csupg --docdir=/usr/csupg-11.21/doc --htmldir=/usr/csupg-11.21/doc/html --with-icu --with-tcl --with-perl --with-python --with-pam --with-openssl --with-libxml --with-libxslt --with-extra-version="\ (CSUDATA.COM CentOS 7)" --enable-nls --enable-integer-datetimes --enable-thread-safety --enable-tap-tests --enable-debug --enable-dtrace --disable-rpath --with-uuid=e2fs --with-gnu-ld --with-system-tzdata=/usr/share/zoneinfo --with-llvm --with-systemd --with-selinux --with-gssapi --with-ldap --with-includes=/usr/include/mit-krb5 --with-libs=/usr/lib/mit-krb5 PYTHON='/usr/bin/python3'  --with-libs=/usr/lib/x86_64-linux-gnu/mit-krb5
```

PostgreSQL 12.16

```sh
./configure --enable-rpath --prefix=/usr/csupg-12.16  --datadir=/usr/csupg-12.16/share --includedir=/usr/csupg-12.16/include --mandir=/usr/csupg-12.16/share/man --libdir=/usr/csupg-12.16/lib --sysconfdir=/etc/sysconfig/csupg --docdir=/usr/csupg-12.16/doc --htmldir=/usr/csupg-12.16/doc/html --with-icu --with-tcl --with-perl --with-python --with-pam --with-openssl --with-libxml --with-libxslt --with-extra-version="\ (CSUDATA.COM CentOS 7)" --enable-nls --enable-integer-datetimes --enable-thread-safety --enable-tap-tests --enable-debug --enable-dtrace --disable-rpath --with-uuid=e2fs --with-gnu-ld --with-system-tzdata=/usr/share/zoneinfo --with-llvm --with-systemd --with-selinux --with-gssapi --with-ldap --with-includes=/usr/include/mit-krb5 --with-libs=/usr/lib/mit-krb5 PYTHON='/usr/bin/python3'  --with-libs=/usr/lib/x86_64-linux-gnu/mit-krb5 AWK='mawk' PROVE='/usr/bin/prove' XSLTPROC='xsltproc --nonet'
```

PostgreSQL 13.12

```sh
./configure --enable-rpath --prefix=/usr/csupg-13.12  --datadir=/usr/csupg-13.12/share --includedir=/usr/csupg-13.12/include --mandir=/usr/csupg-13.12/share/man --libdir=/usr/csupg-13.12/lib --sysconfdir=/etc/sysconfig/csupg --docdir=/usr/csupg-13.12/doc --htmldir=/usr/csupg-13.12/doc/html --with-icu --with-tcl --with-perl --with-python --with-pam --with-openssl --with-libxml --with-libxslt --with-extra-version="\ (CSUDATA.COM CentOS 7)" --enable-nls --enable-integer-datetimes --enable-thread-safety --enable-tap-tests --enable-debug --enable-dtrace --disable-rpath --with-uuid=e2fs --with-gnu-ld --with-system-tzdata=/usr/share/zoneinfo --with-llvm --with-systemd --with-selinux --with-gssapi --with-ldap --with-includes=/usr/include/mit-krb5 --with-libs=/usr/lib/mit-krb5 PYTHON='/usr/bin/python3'  --with-libs=/usr/lib/x86_64-linux-gnu/mit-krb5 AWK='mawk' PROVE='/usr/bin/prove' XSLTPROC='xsltproc --nonet'
```

PostgreSQL 14.9

```sh
./configure --enable-rpath --prefix=/usr/csupg-14.9 --datadir=/usr/csupg-14.9/share --includedir=/usr/csupg-14.9/include --mandir=/usr/csupg-14.9/share/man --libdir=/usr/csupg-14.9/lib --sysconfdir=/etc/sysconfig/csupg --docdir=/usr/csupg-14.9/doc --htmldir=/usr/csupg-14.9/doc/html --with-icu --with-tcl --with-lz4 --with-perl --with-python --with-pam --with-openssl --with-libxml --with-libxslt --with-extra-version="\ (CSUDATA.COM CentOS 7)" --enable-nls --enable-integer-datetimes --enable-thread-safety --enable-tap-tests --enable-debug --enable-dtrace --disable-rpath --with-uuid=e2fs --with-gnu-ld --with-system-tzdata=/usr/share/zoneinfo --with-llvm --with-systemd --with-selinux --with-gssapi --with-ldap --with-includes=/usr/include/mit-krb5 --with-libs=/usr/lib/mit-krb5 PYTHON='/usr/bin/python3'  --with-libs=/usr/lib/x86_64-linux-gnu/mit-krb5 AWK='mawk' PROVE='/usr/bin/prove' XSLTPROC='xsltproc --nonet'
```





