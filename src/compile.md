# 编译安装PostgreSQL

##  编译前注意事项

### 去除postgresql子文件目录
> 默认编译中/usr/csupg-9.6.24/share中会出现postgresql的子目录，把这个子目录去掉的方法如下

```
# 修改postgres源码文件
vi src/Makefile.global.in
# 输入命令，将 "findstring pgsql" => "findstring csupg"
%s/findstring pgsql/findstring csupg/g
```

```
# 指定环境变量
export CFLAGS='-O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches   -m64 -mtune=generic'
```

```
export PATH=/usr/lib64/llvm3.9/bin:$PATH
```


## 各版本编译参数

### potgresql 9.6.24

* PG9.6.24编译参数

````
./configure --enable-rpath --prefix=/usr/csupg-9.6.24 --datadir=/usr/csupg-9.6.24/share --includedir=/usr/csupg-9.6.24/include --mandir=/usr/csupg-9.6.24/share/man --libdir=/usr/csupg-9.6.24/lib --sysconfdir=/etc/sysconfig/csupg --docdir=/usr/csupg-9.6.24/doc --htmldir=/usr/csupg-9.6.24/doc/html --with-tcl --with-perl --with-python --with-pam --with-openssl --with-libxml --with-libxslt --with-extra-version="\ (CSUDATA.COM CentOS 7)" --enable-nls --enable-integer-datetimes --enable-thread-safety --enable-tap-tests --enable-debug --enable-dtrace --disable-rpath --with-uuid=e2fs --with-gnu-ld --with-system-tzdata=/usr/share/zoneinfo --with-systemd --with-selinux --with-gssapi --with-ldap --with-includes=/usr/include/mit-krb5 --with-libs=/usr/lib/mit-krb5 PYTHON='/usr/bin/python3'  --with-libs=/usr/lib/x86_64-linux-gnu/mit-krb5
````

### potgresql 10.20

* 编译参数

```
./configure --enable-rpath --prefix=/usr/csupg-10.20 --datadir=/usr/csupg-10.20/share --includedir=/usr/csupg-10.20/include --mandir=/usr/csupg-10.20/share/man --libdir=/usr/csupg-10.20/lib --sysconfdir=/etc/sysconfig/csupg --docdir=/usr/csupg-10.20/doc --htmldir=/usr/csupg-10.20/doc/html --with-icu --with-tcl --with-perl --with-python --with-pam --with-openssl --with-libxml --with-libxslt --with-extra-version="\ (CSUDATA.COM CentOS 7)" --enable-nls  --enable-integer-datetimes --enable-thread-safety --enable-tap-tests --enable-debug --enable-dtrace --disable-rpath --with-uuid=e2fs --with-gnu-ld --with-system-tzdata=/usr/share/zoneinfo --with-systemd --with-selinux --with-gssapi --with-ldap --with-includes=/usr/include/mit-krb5 --with-libs=/usr/lib/mit-krb5 PYTHON='/usr/bin/python3'  --with-libs=/usr/lib/x86_64-linux-gnu/mit-krb5
```

### potgresql 11.15

* 编译参数

```
./configure --enable-rpath --prefix=/usr/csupg-11.15 --datadir=/usr/csupg-11.15/share --includedir=/usr/csupg-11.15/include --mandir=/usr/csupg-11.15/share/man --libdir=/usr/csupg-11.15/lib --sysconfdir=/etc/sysconfig/csupg --docdir=/usr/csupg-11.15/doc --htmldir=/usr/csupg-11.15/doc/html --with-icu --with-tcl --with-perl --with-python --with-pam --with-openssl --with-libxml --with-libxslt --with-extra-version="\ (CSUDATA.COM CentOS 7)" --enable-nls --enable-integer-datetimes --enable-thread-safety --enable-tap-tests --enable-debug --enable-dtrace --disable-rpath --with-uuid=e2fs --with-gnu-ld --with-system-tzdata=/usr/share/zoneinfo --with-llvm --with-systemd --with-selinux --with-gssapi --with-ldap --with-includes=/usr/include/mit-krb5 --with-libs=/usr/lib/mit-krb5 PYTHON='/usr/bin/python3'  --with-libs=/usr/lib/x86_64-linux-gnu/mit-krb5
```

### potgresql 12.10

* 编译参数

```
./configure --enable-rpath --prefix=/usr/csupg-12.10  --datadir=/usr/csupg-12.10/share --includedir=/usr/csupg-12.10/include --mandir=/usr/csupg-12.10/share/man --libdir=/usr/csupg-12.10/lib --sysconfdir=/etc/sysconfig/csupg --docdir=/usr/csupg-12.10/doc --htmldir=/usr/csupg-12.10/doc/html --with-icu --with-tcl --with-perl --with-python --with-pam --with-openssl --with-libxml --with-libxslt --with-extra-version="\ (CSUDATA.COM CentOS 7)" --enable-nls --enable-integer-datetimes --enable-thread-safety --enable-tap-tests --enable-debug --enable-dtrace --disable-rpath --with-uuid=e2fs --with-gnu-ld --with-system-tzdata=/usr/share/zoneinfo --with-llvm --with-systemd --with-selinux --with-gssapi --with-ldap --with-includes=/usr/include/mit-krb5 --with-libs=/usr/lib/mit-krb5 PYTHON='/usr/bin/python3'  --with-libs=/usr/lib/x86_64-linux-gnu/mit-krb5 AWK='mawk' PROVE='/usr/bin/prove' XSLTPROC='xsltproc --nonet'
```

### potgresql 13.6

* 编译参数

```
./configure --enable-rpath --prefix=/usr/csupg-13.6  --datadir=/usr/csupg-13.6/share --includedir=/usr/csupg-13.6/include --mandir=/usr/csupg-13.6/share/man --libdir=/usr/csupg-13.6/lib --sysconfdir=/etc/sysconfig/csupg --docdir=/usr/csupg-13.6/doc --htmldir=/usr/csupg-13.6/doc/html --with-icu --with-tcl --with-perl --with-python --with-pam --with-openssl --with-libxml --with-libxslt --with-extra-version="\ (CSUDATA.COM CentOS 7)" --enable-nls --enable-integer-datetimes --enable-thread-safety --enable-tap-tests --enable-debug --enable-dtrace --disable-rpath --with-uuid=e2fs --with-gnu-ld --with-system-tzdata=/usr/share/zoneinfo --with-llvm --with-systemd --with-selinux --with-gssapi --with-ldap --with-includes=/usr/include/mit-krb5 --with-libs=/usr/lib/mit-krb5 PYTHON='/usr/bin/python3'  --with-libs=/usr/lib/x86_64-linux-gnu/mit-krb5 AWK='mawk' PROVE='/usr/bin/prove' XSLTPROC='xsltproc --nonet'
```

### potgresql 14.2

* 编译参数

```
./configure --enable-rpath --prefix=/usr/csupg-14.2 --datadir=/usr/csupg-14.2/share --includedir=/usr/csupg-14.2/include --mandir=/usr/csupg-14.2/share/man --libdir=/usr/csupg-14.2/lib --sysconfdir=/etc/sysconfig/csupg --docdir=/usr/csupg-14.2/doc --htmldir=/usr/csupg-14.2/doc/html --with-icu --with-tcl --with-lz4 --with-perl --with-python --with-pam --with-openssl --with-libxml --with-libxslt --with-extra-version="\ (CSUDATA.COM CentOS 7)" --enable-nls --enable-integer-datetimes --enable-thread-safety --enable-tap-tests --enable-debug --enable-dtrace --disable-rpath --with-uuid=e2fs --with-gnu-ld --with-system-tzdata=/usr/share/zoneinfo --with-llvm --with-systemd --with-selinux --with-gssapi --with-ldap --with-includes=/usr/include/mit-krb5 --with-libs=/usr/lib/mit-krb5 PYTHON='/usr/bin/python3'  --with-libs=/usr/lib/x86_64-linux-gnu/mit-krb5 AWK='mawk' PROVE='/usr/bin/prove' XSLTPROC='xsltproc --nonet'

# 较13版本增加
--with-lz4
```

##  数据库测试

### 创建用户postgres 

> 如果未使用yum 或 apt 源安装过的话，需要先创建postgres用户

* 创建用户

  > useradd  -m postgres


### 初始化测试

* 设置用户的环境变量
* 初始化数据库


  ```shell
  yum install postgresql11-server postgresql11-contrib postgresql11-devel
  ```

  ```shell
  yum install epel-release.noarch
  ```

  ```
  yum install centos-release-scl-rh
  ```
  * <font color='red'>指定使用clang-7</font>
  >  scl enable llvm-toolset-7 bash

## yum源安装时提示的依赖安装

```
Installed:
  perl.x86_64 4:5.16.3-299.el7_9            
  perl-Getopt-Long.noarch 0:2.40-3.el7
  perl-Pod-Escapes.noarch 1:1.04-299.el7_9  
  perl-Socket.x86_64 0:2.010-5.el7
  perl-libs.x86_64 4:5.16.3-299.el7_9       
  perl-macros.x86_64 4:5.16.3-299.el7_9
  python3.x86_64 0:3.6.8-18.el7             
  python3-libs.x86_64 0:3.6.8-18.el7
  python3-pip.noarch 0:9.0.3-8.el7          
  python3-setuptools.noarch 0:39.2.0-10.el7

Dependency Installed:
  perl-Carp.noarch 0:1.26-244.el7         
  perl-Encode.x86_64 0:2.51-7.el7
  perl-Exporter.noarch 0:5.68-3.el7       
  perl-File-Path.noarch 0:2.09-2.el7
  perl-File-Temp.noarch 0:0.23.01-3.el7   
  perl-Filter.x86_64 0:1.49-3.el7
  perl-HTTP-Tiny.noarch 0:0.033-3.el7     
  perl-PathTools.x86_64 0:3.40-5.el7
  perl-Pod-Perldoc.noarch 0:3.20-4.el7    
  perl-Pod-Simple.noarch 1:3.28-4.el7
  perl-Pod-Usage.noarch 0:1.63-3.el7      
  perl-Scalar-List-Utils.x86_64 0:1.27-248.el7
  perl-Storable.x86_64 0:2.45-3.el7       
  perl-Text-ParseWords.noarch 0:3.29-4.el7
  perl-Time-HiRes.x86_64 4:1.9725-3.el7   
  perl-Time-Local.noarch 0:1.2300-2.el7
  perl-constant.noarch 0:1.27-2.el7       
  perl-parent.noarch 1:0.225-244.el7
  perl-podlators.noarch 0:2.5.1-3.el7     
  perl-threads.x86_64 0:1.87-4.el7
  perl-threads-shared.x86_64 0:1.43-6.el7
```
